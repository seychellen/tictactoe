export let HU = 'O'
export let AI = 'X'
export let NIL = ''

export const IN_PROGRESS = 'Klick in das Spielfeld...'
export const PLAYER_WINS = 'Du hast gewonnen!'
export const COMPUTER_WINS = 'Du hast leider verloren.'
export const DRAW = 'Das Spiel ist unentschieden.'
export const ERROR = 'Es ist ein Fehler aufgetreten.'
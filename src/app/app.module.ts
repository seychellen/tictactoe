import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {TileComponent} from './tile/tile.component';
import {PlayfieldComponent} from './playfield/playfield.component';
import {ControlComponent} from './control/control.component';
import {FormsModule} from "@angular/forms";

@NgModule({
   declarations: [
      AppComponent,
      TileComponent,
      PlayfieldComponent,
      ControlComponent
   ],
   imports: [
      BrowserModule,
      FormsModule
   ],
   providers: [],
   bootstrap: [AppComponent]
})
export class AppModule {
}

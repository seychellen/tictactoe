import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ControlService} from "../services/control.service";
import {GameStatus} from "../common/GameStatus";


@Component({
   selector: 'app-tile',
   templateUrl: './tile.component.html',
   styleUrls: ['./tile.component.css']
})
export class TileComponent implements OnInit {

   @Input() sign: string = ''
   @Output() signEvent = new EventEmitter<String>()

   constructor(private controlService: ControlService) {
   }

   ngOnInit(): void {
   }

   onClick() {
      if (this.enabled()) {
         this.signEvent.emit(this.sign)
      }
   }

   enabled() {
      return this.sign === '' && this.controlService.gameStatus() === GameStatus.IN_PROGRESS
   }

}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {ControlService} from "../services/control.service";
import {GameStatus} from "../common/GameStatus";
import {IN_PROGRESS, PLAYER_WINS, COMPUTER_WINS, DRAW, ERROR} from "../common/Constants";
import {Subscription} from "rxjs";

@Component({
   selector: 'app-control',
   templateUrl: './control.component.html',
   styleUrls: ['./control.component.css']
})
export class ControlComponent implements OnInit, OnDestroy {

   message: String = IN_PROGRESS
   computerStarts: boolean = false
   private winnerSubscription: Subscription;

   constructor(private controlService: ControlService) {
      this.winnerSubscription = controlService.winner$.subscribe((gameStatus) => {
         switch (gameStatus) {
            case GameStatus.HU_WINS:
               this.message = PLAYER_WINS
               break
            case GameStatus.AI_WINS:
               this.message = COMPUTER_WINS
               break
            case GameStatus.DRAW:
               this.message = DRAW
               break
            case GameStatus.IN_PROGRESS:
               this.message = IN_PROGRESS
               break
            default:
               this.message = ERROR
         }
      })
   }

   ngOnDestroy(): void {
      this.winnerSubscription.unsubscribe()
    }

   ngOnInit(): void {
   }

   onClick() {
      this.controlService.newGame(this.computerStarts)
   }
}

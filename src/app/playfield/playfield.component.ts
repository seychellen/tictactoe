import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {GameService} from "../services/game.service";
import {ControlService} from "../services/control.service";
import {GameStatus} from "../common/GameStatus";
import {Playfield} from "../common/Playfield";
import {HU, AI, NIL} from "../common/Constants";
import {Subscription} from "rxjs";

@Component({
   selector: 'app-playfield',
   templateUrl: './playfield.component.html',
   styleUrls: ['./playfield.component.css']
})
export class PlayfieldComponent implements OnInit, OnDestroy {

   playfield: Playfield = Array(9).fill(NIL)
   private newGameSubscription: Subscription
   @Output() finishEvent = new EventEmitter<string>()

   constructor(private playService: GameService, private controlService: ControlService) {
      this.newGameSubscription = controlService.newGame$.subscribe((computerStart) => {
         this.playfield = Array(9).fill(NIL)
         if (computerStart) {
            this.playfield[Math.floor(Math.random() * 9)] = AI
         }
         this.controlService.changeMessage(GameStatus.IN_PROGRESS)
      })
   }

   ngOnInit(): void {
   }

   ngOnDestroy(): void {
      this.newGameSubscription.unsubscribe()
   }

   onMoveEvent(idx: number) {
      this.playfield[idx] = HU
      if (this.playService.winning(this.playfield, HU)) {
         this.controlService.changeMessage(GameStatus.HU_WINS)
         return
      }
      let emptyIndices = this.playService.emptyIndices(this.playfield)
      // Um dem Spieler überhaupt eine Gewinnchance einzuräumen, wird
      // der zweite Zug zufällig gewählt, falls HU beginnt:
      if (emptyIndices.length === 8) {
         this.playfield[emptyIndices[Math.floor(Math.random() * 8)]] = AI
      }
      else {
         let move = this.playService.nextMove(this.playfield)
         if (move === null) {
            this.controlService.changeMessage(GameStatus.DRAW)
            return
         }
         this.playfield[move] = AI
         if (this.playService.winning(this.playfield, AI)) {
            this.controlService.changeMessage(GameStatus.AI_WINS)
            return
         }
         if (this.playService.nextMove(this.playfield) === null) {
            this.controlService.changeMessage(GameStatus.DRAW)
         }
      }
   }

}

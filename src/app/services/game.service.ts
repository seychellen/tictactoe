import {Injectable} from '@angular/core';
import {Playfield} from "../common/Playfield";
import {HU, AI, NIL} from "../common/Constants";

interface Move {
   index: number | null
   score: number
}

@Injectable({
   providedIn: 'root'
})
export class GameService {

   constructor() {
   }

   nextMove(playfield: Playfield): number | null {

      // 1: Dies musste dann aufgrund von 2 geschehen, da sonst
      // das Verhindern des Gegnergewinns dem eigenen Gewinnen
      // vorgezogen würde
      let myWinningMove = this.simpleWinningMove(playfield, AI)
      if (myWinningMove !== null) return myWinningMove

      // 2: MinMax verhindert komischerweise nicht die einfachen Verluste
      // Aus diesem Grund wird das  zuvor mal abgecheckt:
      let avoidLosingMove = this.simpleWinningMove(playfield, HU)
      if (avoidLosingMove !== null) return avoidLosingMove

      let move = this.minimax(playfield, AI)
      return move.index;
   }

   // returns list of the indexes of empty spots on the board
   emptyIndices(board: Playfield): number[] {
      return [0, 1, 2, 3, 4, 5, 6, 7, 8].filter(i => board[i] !== HU && board[i] !== AI);
   }


   // winning combinations using the board indexies
   winning(board: Playfield, player: string): boolean {
      return (board[0] == player && board[1] == player && board[2] == player) ||
         (board[3] == player && board[4] == player && board[5] == player) ||
         (board[6] == player && board[7] == player && board[8] == player) ||
         (board[0] == player && board[3] == player && board[6] == player) ||
         (board[1] == player && board[4] == player && board[7] == player) ||
         (board[2] == player && board[5] == player && board[8] == player) ||
         (board[0] == player && board[4] == player && board[8] == player) ||
         (board[2] == player && board[4] == player && board[6] == player);
   }


   simpleWinningMove(board: Playfield, player: string): number | null {
      let emptyIndices = this.emptyIndices(board);
      for (let i of emptyIndices) {
         board[i] = player
         let huWouldWin = this.winning(board, player);
         board[i] = NIL
         if (huWouldWin) return i;
      }
      return null
   }

   minimax(newBoard: Playfield, player: string): Move {

      let availSpots = this.emptyIndices(newBoard);
      if (this.winning(newBoard, HU)) {
         return {index: null, score: -10};
      }
      else if (this.winning(newBoard, AI)) {
         return {index: null, score: 10};
      }
      else if (availSpots.length === 0) {
         return {index: null, score: 0};
      }

      let moves = [];

      for (let i = 0; i < availSpots.length; i++) {
         //create an object for each and store the index of that spot
         let move: Move = {
            index: availSpots[i],
            score: 0
         };
         // set the empty spot to the current player
         newBoard[availSpots[i]] = player;

         // collect the score resulted from calling minimax
         // on the opponent of the current player
         if (player === AI) {
            let result = this.minimax(newBoard, HU);
            move.score = result.score;
         } else {
            let result = this.minimax(newBoard, AI);
            move.score = result.score;
         }
         // reset the spot to empty
         newBoard[availSpots[i]] = NIL;

         // push the object to the array
         moves.push(move);
      }

      let bestMoveIdx = -1
      if (player === AI) {
         let bestScore = -10000;
         for (let i = 0; i < moves.length; i++) {
            if (moves[i].score > bestScore) {
               bestScore = moves[i].score;
               bestMoveIdx = i;
            }
         }
      } else {
         // else loop over the moves and choose the move with the lowest score
         let bestScore = 10000;
         for (let i = 0; i < moves.length; i++) {
            if (moves[i].score < bestScore) {
               bestScore = moves[i].score;
               bestMoveIdx = i;
            }
         }
      }
      // return the chosen move (object) from the moves array
      return moves[bestMoveIdx];
   }

}

import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {GameStatus} from "../common/GameStatus";

@Injectable({
   providedIn: 'root'
})
export class ControlService {

   constructor() {
   }

   readonly winner$  = new BehaviorSubject<GameStatus>(GameStatus.IN_PROGRESS)

   gameStatus(): GameStatus {
      return this.winner$.value
   }


   changeMessage(gameStatus: GameStatus) {
      this.winner$.next(gameStatus)
   }

   newGameSource = new BehaviorSubject<boolean>(false)
   newGame$ = this.newGameSource.asObservable()

   newGame(computerStarts: boolean) {
      this.newGameSource.next(computerStarts)
   }


}
